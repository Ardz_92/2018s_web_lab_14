<%--
  Created by IntelliJ IDEA.
  User: Andrew Meads
  Date: 13/05/2018
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Web Lab 17</title>
</head>
<body>
<ul>
    <li><a href="ImageGalleryDisplay">Image Gallery</a></li>
    <li><a href="Exercise01/exercise01-01.jsp">Exercise 01-01</a></li>
    <li><a href="Exercise01/exercise01-02.jsp">Exercise 01-02</a></li>
    <li><a href="Exercise02/exercise02-01.jsp">Exercise 02-01</a></li>
    <li><a href="Exercise02/exercise02-02.jsp">Exercise 02-02</a></li>
    <li><a href="Exercise03/ex08/edmund_hillary.html">Exercise 03</a></li>
    <li><a href="WEB-INF/Exercise04.jsp">Exercise 04</a></li>
</ul>
</body>
</html>
